package projet_fixe;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author dboudat
 */
public class MOCN {

    private SimpleIntegerProperty ID;
    private SimpleStringProperty Date, Nom, Etat;

    /**
     * Construit un nouveau objet MOCN
     *
     * @param id Id de la MOCN
     * @param etat Etat actuel de la MOCN
     * @param date Date et heure de la mesure
     * @param nom Nom de la MOCN
     */
    public MOCN(long id, long etat, String date, String nom) {

        this.ID = new SimpleIntegerProperty((int) id);
        this.Date = new SimpleStringProperty(date);
        this.Nom = new SimpleStringProperty(nom);

        switch ((int) etat) {
            case -1:
                this.Etat = new SimpleStringProperty("Non Connectée");
                break;
            case 0:
                this.Etat = new SimpleStringProperty("Arrêt");
                break;
            case 2:
                this.Etat = new SimpleStringProperty("Production normale");
                break;
            default:
                this.Etat = new SimpleStringProperty("Production Dégradée");
                break;
        }
    }

    /**
     * Retourne la valeur de l'attribut ID
     *
     * @return l'id de la MOCN
     */
    public int getID() {
        return ID.get();
    }

    /**
     * Fixe la valeur de m'attribut ID
     *
     * @param ID Retourne la valeur de l'attribut ID
     */
    public void setID(int ID) {
        this.ID.set(ID);
    }

    /**
     * Retourne la valeur de l'attribut Etat
     *
     * @return l'etat de la MOCN
     */
    public String getEtat() {
        return Etat.get();
    }

    /**
     * Fixe la valeur de l'attribut Etat
     *
     * @param Etat Etat de la MOCN
     */
    public void setEtat(String Etat) {
        this.Etat.set(Etat);
    }

    /**
     * Retourne la valeur de l'attribut Date
     *
     * @return date de la dernière mesure
     */
    public String getDate() {
        return Date.get();
    }

    /**
     * Fixe la valeur de l'attribut Date
     *
     * @param Date date de la dernière mesure
     */
    public void setDate(String Date) {
        this.Date.set(Date);
    }

    /**
     * Retourne la valeur de l'attribut Nom
     *
     * @return le nom de la MOCN
     */
    public String getNom() {
        return Nom.get();
    }

    /**
     * Fixe la valeur de l'attribut Nom
     *
     * @param Nom nom de la MOCN
     */
    public void setNom(String Nom) {
        this.Nom.set(Nom);
    }
}
