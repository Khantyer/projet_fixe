package projet_fixe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProxySelector;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author dboudat
 */
public class AgentJSON extends Thread {

    private URL Adresse;
    private IhmController Controller;

    /**
     * Constructeur de la classe AgentJSON
     *
     * @param ctrl Reference ver l'objet IhmController
     * @param adresse Chaine de caractere de l'adresse du serveur
     */
    public AgentJSON(IhmController ctrl, String adresse) {
        setName("AgentJSON");
        this.Controller = ctrl;
        setAdresse(adresse);
    }

    /**
     * Recupere la trame JSON et declenche la mise a jour de l'ihm toute les
     * minutes
     */
    @Override
    public void run() {
        while (!AgentJSON.interrupted()) {
            try {
                Controller.majIHM(ReceptionJSON());
                AgentJSON.sleep(60000);
            } catch (InterruptedException ex) {
                if (!"sleep interrupted".equals(ex.getMessage())) {
                    Logger.getLogger(AgentJSON.class.getName()).log(Level.SEVERE, null, ex);
               }
            }
        }
    }

    /**
     * Convertit la trame JSON en tableau d'objet MOCN
     *
     * @return retourne un tableau d'objet MOCN ou null en cas d'erreur
     */
    private ArrayList<MOCN> ReceptionJSON() {
        ArrayList<MOCN> TabMOCN = new ArrayList();
        try {
            ProxySelector.setDefault(null);// pour s'affranchir du proxy

            String reponse = Httpget(); //Demande du contenu de la page web
            System.out.println("Reponse : " + reponse);
            JSONParser Json = new JSONParser();

            Object obj = Json.parse(reponse); //Lecture de l'objet JSON
            JSONArray Tableau = (JSONArray) obj; //Mise en place d'un tableau d'objet

            for (int i = 0; i < Tableau.size(); i++) {
                JSONObject JsonObj = (JSONObject) Tableau.get(i); //Creation d'un objet avec une case du tableau
                TabMOCN.add(new MOCN((long) JsonObj.get("id"),
                        (long) JsonObj.get("etat"),
                        (String) JsonObj.get("date"),
                        (String) JsonObj.get("nom")));
            }
            return TabMOCN;
        } catch (ParseException ex) {
            Logger.getLogger(AgentJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Recupere le contenu de la page du web service et la retourne sous forme
     * de chaine de caractère
     *
     * @return Chaine de caractère du contenu de la page ou null si erreur
     */
    private String Httpget() {
        try {
            HttpURLConnection connect = (HttpURLConnection) Adresse.openConnection(); //Connexion a l'url
            if (connect.getResponseCode() != 200) {
                System.err.println("Echec de la connexion à l'url : " + Adresse.toString());
                throw new IOException(connect.getResponseMessage()); //Affiche le message d'erreur dans le cas ou la connexion echoue
            }
            BufferedReader rd = new BufferedReader(new InputStreamReader(connect.getInputStream())); //Creation d'un buffer pour chaine de caractere
            StringBuilder Sb = new StringBuilder();
            String Line;
            while ((Line = rd.readLine()) != null) {
                Sb.append(Line); //Ajoute une nouvelle ligne
            }
            rd.close();
            connect.disconnect();
            return Sb.toString();
        } catch (IOException ex) {
            Logger.getLogger(AgentJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Recupère l'adresse sous la forme d'un objet URL
     *
     * @return objet URL
     */
    public URL getAdresse() {
        return Adresse;
    }

    /**
     * Fixe la valeur de l'attribut adresse
     *
     * @param adresse Chaine de caractere contenant l'adresse
     */
    public final void setAdresse(String adresse) {
        try {
            this.Adresse = new URL("http://" + adresse + "/ws/app/refresh/");
        } catch (MalformedURLException ex) {
            Logger.getLogger(AgentJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
