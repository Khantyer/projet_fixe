package projet_fixe;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * @author dboudat
 */
public class IhmController implements Initializable {

    @FXML
    private TextField TF_Adresse;
    @FXML
    private Button Btn_Connexion;
    @FXML
    private TableView<MOCN> table_mocn;
    @FXML
    private TableColumn Colonne_Date, Colonne_ID, Colonne_Nom, Colonne_Etat;

    private ObservableList<MOCN> data_mocn = FXCollections.observableArrayList();
    private final static String Adresse_Defaut = "172.17.1.202";
    private boolean Connecte = false;
    AgentJSON agent;

    /**
     * Initialise le contenu de l'IHM
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Colonne_Date.setCellValueFactory(new PropertyValueFactory<MOCN, String>("Date"));
        Colonne_Date.setVisible(true);
        Colonne_Nom.setCellValueFactory(new PropertyValueFactory<MOCN, String>("Nom"));
        Colonne_Nom.setVisible(true);
        Colonne_ID.setCellValueFactory(new PropertyValueFactory<MOCN, String>("ID"));
        Colonne_ID.setVisible(true);
        Colonne_Etat.setCellValueFactory(new PropertyValueFactory<MOCN, String>("Etat"));
        Colonne_Etat.setVisible(true);
        TF_Adresse.setText(Adresse_Defaut);
    }

    /**
     * Mise a jour du contenu de l'IHM
     *
     * @param TabMOCN Tableau d'objet MOCN
     */
    public void majIHM(ArrayList<MOCN> TabMOCN) {
        data_mocn.clear();
        for (int i = 0; i < TabMOCN.size(); i++) {
            data_mocn.add(TabMOCN.get(i));
        }
        table_mocn.setItems(data_mocn);
    }

    /**
     * Methode se declenchant lors de l'appui sur le bouton connexion
     *
     * @param event
     */
    public void HandleBtn_Connexion(Event event) {
        if (isConnecte()) {
            agent.interrupt();
            TF_Adresse.setEditable(true);
            Btn_Connexion.setText("Connexion");
            Connecte = false;
        } else {
            agent = new AgentJSON(this, TF_Adresse.getText());
            TF_Adresse.setEditable(false);
            Btn_Connexion.setText("Déconnexion");
            agent.start(); //Demarrage du thread
            Connecte = true;
        }
    }

    public boolean isConnecte() {
        return Connecte;
    }
    
}
